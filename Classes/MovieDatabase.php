<?php
/*
    This is a media database to mange your Movie.
    Copyright (C) 2017 Nick Tranholm Asselberghs

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require('MediaDatabase.php');


class MovieDatabase extends MediaDatabase
{
    //showTable
    protected $table_stmt;
    protected $row;
    //addMovie
    protected $title;
    protected $production_year;
    protected $actor;
    protected $director;
    protected $genre;
    protected $price;
    protected $formatdata;



    public function __construct($server, $user, $password, $dbname)
    {
       parent::__construct($server, $user, $password, $dbname);
    }

    public function showTable($search = '', $field = '') {
        if ($search == '' && $field == '') {
            $this->table_stmt = $this->db->prepare("SELECT * FROM Movie");
        } else {
            $this->table_stmt = $this->db->prepare("SELECT * FROM Movie WHERE $field LIKE '%$search%'");
        }

        try {
            $this->table_stmt->execute();
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
        echo '<center>';
        echo '<table border="1">';
        echo '<tr>';
        echo '<td><p>Titel</p></td><td><p>Genre</p></td><td><p>Productions år</p></td><td><p>Skuespillere</p></td><td><p>Instruktør</p></td><td><p>Format</p></td><td><p>Ejer</p></td>';
        if($_SESSION['Logged_In']) {
           echo '<td><p>Pris</p></td>';
        }
        echo '</tr>';

        while($this->row = $this->table_stmt->fetch(PDO::FETCH_OBJ)) {
            $lend_status = $this->row->Lend;
            if($lend_status == 'Yes') {
            echo '<tr class="borrowed">';
            } else {
                echo '<tr>';
            }
            $User = $this->row->User;
            $User = ucfirst($User);
            echo '<td>'.$this->row->Title.'</td>';
            echo '<td>'.$this->row->Genre.'</td>';
            echo '<td>'.$this->row->Production_Year.'</td>';
            echo '<td>'.$this->row->Actor.'</td>';
            echo '<td>'.$this->row->Director.'</td>';
            echo '<td>'.$this->row->Format.'</td>';
            echo '<td>'.$User.'</td>';
            if($_SESSION['Logged_In']) {
                echo "<td>".$this->row->Price."</td>";
                echo "<td><a href=\"update.php?Title=" . $this->row->Title . "&ID=" . $this->row->ID . "&Genre=" . $this->row->Genre . "&Production_Year=" . $this->row->Production_Year . "&Actor=" . $this->row->Actor . "&Director=" . $this->row->Director . "&Price=" . $this->row->Price . "\">Update</a></td>";
                echo "<td><a href=\"delete.php?Title=" . $this->row->Title . "\">Delete</a></td>";

                if($lend_status == 'Yes') {
                    echo '<td>'.$this->row->Loaner.'</td>';
                }
            }

            echo '</tr>';

        }

        echo '</table>';
    }

    public function showAddMovie() {
        if($_SESSION['Logged_In']) {

            echo '<form name="login" action="' . $_SERVER['PHP_SELF'] . '" method="post">';
            echo '<p>Titel: <input type="text" name="Title"><br><br>';
            echo 'Produktions Aar: <input type="text" name="Production_Year"><br><br>';
            echo 'Skuespillere: <textarea rows="2" cols="20" name="Actor">';
            echo '</textarea><br><br><br>';
            echo 'Instruktoeer: <textarea rows="2" cols="20" name="Director">';
            echo '</textarea><br><br><br>';
            echo 'Genre: <input type="text" name="Genre"><br><br>';
            echo 'Pris: <input type="text" name="Price"><br><br>';
            echo '<p>Format:</p>';
            echo '<input type="checkbox" name="FormatCheck[]" id="DVD" value="DVD"> <label for="DVD">DVD</label><br />';
            echo '<input type="checkbox" name="FormatCheck[]" id="Blu-Ray" value="Blu-Ray"> <label for="Blu-Ray">Blu-Ray</label><br />';
            echo '</p>';
            echo '<input type="submit" name="submit" value="Add"><br />';
            echo '</form>';

            $this->addMovie($_POST['Title'], $_POST['Production_Year'], $_POST['Actor'], $_POST['Director'], $_POST['Genre'], $_POST['Price'], $_POST['FormatCheck']);

        } else {
            echo 'You need to login to be able to add a movie to the database.';
        }
    }

    public function showUpdateMovie($ID,$Title,$Genre,$Production_Year,$Actor,$Director,$Price) {
        if($_SESSION['Logged_In']) {
            echo '<form name="login" action="' . $_SERVER['PHP_SELF'] . '" method="post">';
            echo '<p>Titel: </p><input type="text" name="Title" value="' . $Title . '"><br><br>';
            echo '<p>Produktions &Aring;r: </p><input type="text" name="Production_Year" value="' . $Production_Year . '"><br><br>';
            echo '<p>Skuespillere: </p><textarea rows="2" cols="20" name="Actor">';
            echo $Actor;
            echo '</textarea><br><br><br>';
            echo '<p>Instruktoeer: </p><textarea rows="2" cols="20" name="Director">';
            echo $Director;
            echo '</textarea><br><br><br>';
            echo '<p>Genre: </p><input type="text" name="Genre" value="' . $Genre . '"><br><br>';
            echo '<p>Format: </p>';
            echo '<input type="checkbox" name="FormatCheck[]" id="DVD" value="DVD"> <label for="DVD">DVD</label><br />';
            echo '<input type="checkbox" name="FormatCheck[]" id="Blu-Ray" value="Blu-Ray"> <label for="Blu-Ray">Blu-Ray</label><br />';
            echo '<p>Price: </p><input type="text" name="Price" value="' . $Price . '"><br><br>';

            echo '<p>Udlaant?</p><select name="Lend">';
            echo '<option value="Yes">Yes</option>';
            echo '<option value="No" selected="selected">No</option>';
            echo '</select><br><br>';

            echo '<p>Udlaant til: </p><input type="text" name="Loaner" value="' . $Loaner . '"><br><br>';
            echo '<input type="hidden" name="ID" value="' . $ID . '"><br>';
            echo '<input type="submit" name="submit" value="Opdater"><br />';

            $this->updateMovie($_POST['ID'], $_POST['Title'], $_POST['Production_Year'], $_POST['Actor'], $_POST['Director'], $_POST['Genre'], $_POST['FormatCheck'], $_POST['Price'], $_POST['Lend'], $_POST['Loaner']);
        } else {
            echo 'You need to be logged in to be able to update a movie';
        }
    }

    public function showSearch() {
        parent::showSearch();
        $this->showTable($_POST['Search'],$_POST['Type']);
    }

    public function showRestore(){
        parent::showRestore();
        $this->restore($_POST['restore']);
    }

    private function updateMovie($ID, $title, $production_year, $actor, $director, $genre, $formatcheck, $price, $lend, $loaner) {
        @$formatdata = implode(",", $formatcheck);


        $update_query_title = $this->db->prepare("UPDATE Movie SET Movie.Title = :title WHERE Movie.ID = :id");
        $update_query_title->bindParam(':title', $title, PDO::PARAM_STR);
        $update_query_title->bindParam(':id', $ID, PDO::PARAM_INT);
        try{
            $update_query_title->execute();
        }catch (PDOException $e) {
            echo $e->getMessage();
        }

        $update_query_production_year = $this->db->prepare("UPDATE Movie SET Movie.Production_Year = :production_year WHERE Movie.ID = :id");
        $update_query_production_year->bindParam(':production_year', $production_year, PDO::PARAM_STR);
        $update_query_production_year->bindParam(':id', $ID, PDO::PARAM_INT);
        try{
            $update_query_production_year->execute();
        }catch (PDOException $e) {
            echo $e->getMessage();
        }

        $update_query_actor = $this->db->prepare("UPDATE Movie SET Movie.Actor = :actor WHERE Movie.ID = :id");
        $update_query_actor->bindParam(':actor', $actor, PDO::PARAM_STR);
        $update_query_actor->bindParam(':id', $ID, PDO::PARAM_INT);
        try {
            $update_query_actor->execute();
        }catch(PDOException $e) {
            echo $e->getMessage();
        }

        $update_query_director = $this->db->prepare("UPDATE Movie SET Movie.Director = :director WHERE Movie.ID = :id");
        $update_query_director->bindParam(':director', $director, PDO::PARAM_STR);
        $update_query_director->bindParam(':id', $ID, PDO::PARAM_INT);
        try{
            $update_query_director->execute();
        }catch(PDOException $e) {
            echo $e->getMessage();
        }

        $update_query_genre = $this->db->prepare("UPDATE Movie SET Movie.Genre = :genre WHERE Movie.ID = :id");
        $update_query_genre->bindParam(':genre', $genre, PDO::PARAM_STR);
        $update_query_genre->bindParam(':id', $ID, PDO::PARAM_INT);
        try{
            $update_query_genre->execute();
        }catch(PDOException $e) {
            echo $e->getMessage();
        }

        $update_query_formatcheck = $this->db->prepare("UPDATE Movie SET Movie.Format = :format WHERE Movie.ID = :id");
        $update_query_formatcheck->bindParam(':format', $formatdata);
        $update_query_formatcheck->bindParam(':id', $ID, PDO::PARAM_INT);
        try{
            $update_query_formatcheck->execute();
        }catch(PDOException $e){
            echo $e->getMessage();
        }

        $update_query_price = $this->db->prepare("UPDATE Movie SET Movie.Price = :price WHERE Movie.ID = :id");
        $update_query_price->bindParam(':price', $price);
        $update_query_price->bindParam(':id', $ID, PDO::PARAM_INT);
        try{
            $update_query_price->execute();
        }catch (PDOException $e) {
            echo $e->getMessage();
        }

        $update_query_lend = $this->db->prepare("UPDATE Movie SET Movie.Lend = :lend WHERE Movie.Lend = :lend");
        $update_query_lend->bindParam(':lend', $lend);
        $update_query_lend->bindParam(':id', $ID, PDO::PARAM_INT);
        try{
            $update_query_lend->execute();
        }catch (PDOException $e) {
            echo $e->getMessage();
        }

        $update_query_loaner = $this->db->prepare("UPDATE Movie SET Movie.Loaner = :loaner WHERE Movie.Loaner = :loaner");
        $update_query_loaner->bindParam(':loaner', $loaner);
        $update_query_loaner->bindParam(':id', $ID, PDO::PARAM_INT);
        try{
            $update_query_loaner->execute();
        }catch (PDOException $e) {
            echo $e->getMessage();
        }

        echo 'Movie has been updated';
    }

    private function addMovie($title, $production_year, $actor, $director, $genre, $price, $format) {
        $this->title = $title;
        $this->production_year = $production_year;
        $this->actor = $actor;
        $this->director = $director;
        $this->genre = $genre;
        $this->price = (int)$price;
        /*TODO Why does implode fail? Error msg: Invalid arguments passed in*/
        @$this->formatdata = implode(",", $format);


        $query_for_movie = $this->db->prepare("SELECT * FROM Movie WHERE Title LIKE :title");
        $query_for_movie->bindParam(":title", $this->title, PDO::PARAM_STR);

            try{
                $query_for_movie->execute();
            }catch (PDOException $e) {
                echo $e->getMessage();
            }

            $titlecheck = "";

            while($row = $query_for_movie->fetch(PDO::FETCH_OBJ)) {
                $titlecheck = $row->Title;
            }

            if($titlecheck!=$this->title) {
                $insert_statement = $this->db->prepare("INSERT INTO Movie (Title, Format,Production_Year,Actor,Director,Genre,Price,User) VALUES (:title,:format,:production_year,:actor,:director,:genre,:price,:user)");
                $insert_statement->bindParam(':title',$this->title,PDO::PARAM_STR);
                $insert_statement->bindParam(':format',$this->formatdata,PDO::PARAM_STR);
                $insert_statement->bindParam(':production_year',$this->production_year, PDO::PARAM_STR);
                $insert_statement->bindParam(':actor',$this->actor,PDO::PARAM_STR);
                $insert_statement->bindParam(':director',$this->director,PDO::PARAM_STR);
                $insert_statement->bindParam(':genre',$this->genre,PDO::PARAM_STR);
                $insert_statement->bindParam(':price',$this->price,PDO::PARAM_INT);
                $insert_statement->bindParam(':user',$_SESSION['User'],PDO::PARAM_STR);

                try{
                    $insert_statement->execute();
                }catch (PDOException $e) {
                    echo $e->getMessage();
                }

                echo 'Filmen er tilføjet til databasen';
            } else {
                echo 'Filmen findes allerede i databasen';
            }
    }

    public function deleteMovie($Title) {
        $delete_query_statement = $this->db->prepare('DELETE FROM Movie WHERE Title = :title');
        $delete_query_statement->bindParam(':title',$Title);
        try {
            $delete_query_statement->execute();
        }catch (PDOException $e) {
            echo $e->getMessage();
        }

        echo 'The movie "'.$Title.'" has been deleted.';
    }
}
?>